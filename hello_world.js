var HelloWorld = (function () {
    function HelloWorld(message) {
        this.message = message;
    }
    return HelloWorld;
})();
var hello = new HelloWorld('Hello TypeScript');
console.log(hello);
/*

Basic Types
-----------
Boolean
Number
String
Array
Enum
Any - The variable can refer to any type
Void - Absence of a type



 */
//# sourceMappingURL=hello_world.js.map