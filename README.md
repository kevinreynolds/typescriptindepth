# TypeScript

Scalable JavaScript development with types, classes and modules.

## Install

  npm install -g typescript

## Usage

  tsc app.ts